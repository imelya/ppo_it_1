#!/usr/bin/env python3
import dash
import dash_core_components as dcc
import dash_html_components as html
from get_live_data import get_temp
import get_data as gd 
from config import number_of_areas, number_of_aparts, number_of_houses

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
app.config.suppress_callback_exceptions = True
server = app.server
app.layout = html.Div([
    dcc.Location(id='url', refresh=False),
    html.Div(id='page-content')
])

links = [dcc.Link('Испытание 1', href='/task1'),
    html.Br(),
    dcc.Link('Испытание 2', href='/task2'),
    html.Br(),
    dcc.Link('Испытание 3', href='/task3'),
    html.Br(),
    dcc.Link('Испытание 4', href='/task4'),
    html.Br(),
    dcc.Link('Испытание 5', href='/task5'),
    html.Br(),
    dcc.Link('Дополнительное испытание', href='/task6'),
    html.Br(),
    dcc.Link('В начало', href='/'),
    html.Br(),
    ]

hello = [html.H2('Москвовская Предрофессиональная олимпиада'),
        html.H3('Профиль: "Информационные технологии"'),
        html.H4('Команда: 11"И"'),
        html.H4('Школа: №1298 "Профиль Куркино"'),
        html.H4('Кейс: №5 "Сбор и обработка данных температуры"'),
        ]

index_page = html.Div(
        hello + links[:-2]
)

task1_layout = html.Div([
    html.H3('Испытание 1'),
    html.H5('Город'),
    dcc.Dropdown(
        id='task_1_city',
        options=gd.get_cities(),
        value='',
    ),
    html.H5('Район'),
    dcc.Input(id='task_1_area', type='number', value=''),
    html.H5('Дом'),
    dcc.Input(id='task_1_house', type='number', value=''),
    html.H5('Квартира'),
    dcc.Input(id='task_1_apart', type='number', value=''),
    html.H5('Температура:'),
    html.Div(id='data'),
    html.Br(),
] + links)

@app.callback(dash.dependencies.Output('data', 'children'),
              [dash.dependencies.Input('task_1_city', 'value'),
              dash.dependencies.Input('task_1_area', 'value'),
              dash.dependencies.Input('task_1_house', 'value'),
              dash.dependencies.Input('task_1_apart', 'value'),
                  ])

def task1_update(city, area, house, apart):
    if type(city) == int and type(area) == int and type(house) == int and type(apart):
        out = get_temp(city, area, house, apart)
    else:
        out = ''
    return out

task2_layout = html.Div([
    html.H3('Испытание 2'),
    html.H5('Город'),
    dcc.Dropdown(
        id='task_2_city',
        options=gd.get_cities(),
        value=None,
    ),
    dcc.Graph(id='task_2_graph',)
    ] + links)

@app.callback(dash.dependencies.Output('task_2_graph', 'figure'),
              [dash.dependencies.Input('task_2_city', 'value'),]
              )

def task2_update(city):
    if not city:
        return {}
    elif type(city) == int:
        data = gd.get_temp_by_city(city)
    return {'data':[
        {'y': data}
        ],
        'layout':dict(title='Изменение показаний уличной температуры в городе ' + gd.get_city_name(city),
            xaxis={'title': 'День'},
            yaxis={'title': 'Температура'})
        }
    
task3_layout = html.Div([
    html.H3('Испытание 3'),
    html.H5('Город'),
    dcc.Dropdown(
        id='task_3_city',
        options=gd.get_cities(),
        value=None,
    ),
    dcc.Graph(id='task_3_graph',)
    ] + links)

@app.callback(dash.dependencies.Output('task_3_graph', 'figure'),
              [dash.dependencies.Input('task_3_city', 'value'),]
              )

def task3_update(city):
    if not city:
        return {}
    elif type(city) == int:
        data = gd.get_aparts_temp_by_city(city)
    return {'data':[
        {'y': data}
        ],
        'layout':dict(title='Изменение средней температуры в квартирах в городе ' + gd.get_city_name(city),
            xaxis={'title': 'День'},
            yaxis={'title': 'Температура'},)
        }

task4_layout = html.Div([
    html.H3('Испытание 4'),
    html.H5('Город'),
    dcc.Dropdown(
        id='task_4_city',
        options=gd.get_cities(),
        value=None,
    ),
    html.H5('Район'),
    dcc.Dropdown(
        id='task_4_area',
        options=gd.get_dd_list(number_of_areas),
        value=None,
    ),
    html.H5('Дом'),
    dcc.Dropdown(
        id='task_4_house',
        options=gd.get_dd_list(number_of_houses),
        value=None,
    ),
    html.H5('Квартира'),
    dcc.Dropdown(
        id='task_4_apart',
        options=gd.get_dd_list(number_of_aparts),
        value=None,
    ),
    dcc.Graph(id='task_4_graph',)
    ] + links)

@app.callback(dash.dependencies.Output('task_4_graph', 'figure'),
              [dash.dependencies.Input('task_4_city', 'value'),
              dash.dependencies.Input('task_4_area', 'value'),
              dash.dependencies.Input('task_4_house', 'value'),
              dash.dependencies.Input('task_4_apart', 'value'),]
              )

def task4_update(city, area, house, apart):
    if not city or not area or not apart or not house:
        return {}
    else:
        data = gd.get_one_apart_temp_by_city(city, area, house, apart)
    return {'data':[
        dict(y=data,
            config=dict(showAxisDragHandles=False)
            )
        ],
        'layout':dict(title = f'Изменение температуры, в квартире {apart}, в {house} доме, в {area} райное, в городе {gd.get_city_name(city)}',
            xaxis={'title': 'День'},
            yaxis={'title': 'Температура'})
        }

task5_layout = html.Div([
    html.H3('Испытание 5'),
    html.H5('Город'),
    dcc.Dropdown(
        id='task_5_city',
        options=gd.get_cities(),
        value=None,
    ),
    dcc.Graph(id='task_5_graph',)
    ] + links)

@app.callback(dash.dependencies.Output('task_5_graph', 'figure'),
              [dash.dependencies.Input('task_5_city', 'value'),]
              )

def task5_update(city):
    if not city:
        return {}
    else:
        data = gd.get_max_temp_by_city_and_area(city)
    return {'data':[
        dict(y=data,
            x=[i for i in range(1, number_of_areas + 1)],
            width=0.05,
            type='bar',
            config=dict(showAxisDragHandles=False))
        ],
        'layout':dict(
            width=1,
            title=f'Максимальная температура в квартирах в каждом из районов города {gd.get_city_name(city)}',
            xaxis={'title': 'Район'},
            yaxis={'title': 'Температура'},), 
        }

task6_layout = html.Div([
    html.H3('Дополнительное испытание'),
    html.H5('Город'),
    dcc.Dropdown(
        id='task_6_city',
        options=gd.get_cities(),
        value=None,
    ),
    dcc.Graph(id='task_6_graph',)
    ] + links)

@app.callback(dash.dependencies.Output('task_6_graph', 'figure'),
              [dash.dependencies.Input('task_6_city', 'value'),]
              )

def task6_update(city):
    if not city:
        return {}
    elif type(city) == int:
        data_1 = gd.get_aparts_temp_by_city(city)
        data_2 = gd.get_temp_by_city(city)
    return {'data':[
        {'y': data_1,
            'name':'В квартирах (средняя)',
            'marker': dict(color='rgb(255, 127, 80)')},
        {'y': data_2,
            'name':'На улице',
            'marker': dict(color='rgb(70, 130, 180)')},
        ],
        'layout':dict(title=f'Температура в городе {gd.get_city_name(city)} и в квартирах (средняя по городу)',
            xaxis={'title': 'День'},
            yaxis={'title': 'Температура'},)
        }

@app.callback(dash.dependencies.Output('page-content', 'children'),
              [dash.dependencies.Input('url', 'pathname')])
def display_page(pathname):
    if pathname == '/task1':
        return task1_layout
    elif pathname == '/task2':
        return task2_layout
    elif pathname == '/task3':
        return task3_layout
    elif pathname == '/task4':
        return task4_layout
    elif pathname == '/task5':
        return task5_layout
    elif pathname == '/task6':
        return task6_layout
    else:
        return index_page

if __name__ == '__main__':
    app.run_server(debug=True)
#    server.run(host='0.0.0.0', port=8000, debug=False)
