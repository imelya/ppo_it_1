import sqlite3
import config

DB_PATH = config.DB_PATH

def get_city_name(city):
    conn = sqlite3.connect(DB_PATH)
    cursor = conn.cursor()
    cursor.execute(f'select name from {config.table_name_1} where id=?', [(city)])
    out = cursor.fetchone()
    cursor.close()
    if len(out) == 1:
        return out[0]
    else:
        return ''

def get_dd_list(number):
    out = []
    dd_list = [i for i in range(1, number + 1)]
    for i in dd_list:
        out.append({'label': str(i), 'value': i})
    return out

def get_cities():
    conn = sqlite3.connect(DB_PATH)
    out = []
    cursor = conn.cursor()
    cursor.execute(f'select * from {config.table_name_1}')
    for i in cursor.fetchall():
        out.append({'label': i[1], 'value': i[0]})
    cursor.close()
    return out

def get_temp_by_city(city):
    conn = sqlite3.connect(DB_PATH)
    cursor = conn.cursor()
    cursor.execute(f'select temp from {config.table_name_2} where city=?', [(city)])
    pre_out = cursor.fetchall()
    cursor.close()
    out = list(map(lambda d: d[0], pre_out))
    return out

def get_aparts_temp_by_city(city):
    conn = sqlite3.connect(DB_PATH)
    cursor = conn.cursor()
    cursor.execute(f'select * from {config.table_name_3} where city=?', [(city)])
    pre_out = cursor.fetchall()
    cursor.close()
    out = []
    f = 0
    n = config.number_of_areas * config.number_of_aparts
    for i in range(int(len(pre_out)/n)):
        out.append(round(sum(list(map(lambda d: d[5], pre_out[f:f+n])))/n, 2))
        f += n
    return out

def get_one_apart_temp_by_city(city, area, house, apart):
    conn = sqlite3.connect(DB_PATH)
    cursor = conn.cursor()
    cursor.execute(f'select temp from {config.table_name_3} where city=? and area=? and house=? and apart=?', [(city), (area), (house), (apart)])
    pre_out = cursor.fetchall()
    cursor.close()
    out = list(map(lambda d: d[0], pre_out))
    return out

def get_max_temp_by_city_and_area(city):
    conn = sqlite3.connect(DB_PATH)
    cursor = conn.cursor()
    out = []
    for area in range(1, config.number_of_areas + 1):
        cursor.execute(f'select temp from {config.table_name_3} where city=? and area=?', [(city), (area)])
        area_data = cursor.fetchall()
        pre_out = max(list(map(lambda d: d[0], area_data)))
        out.append(pre_out)
    cursor.close()
    return out

if __name__ == '__main__':
#    print(get_aparts_temp_by_city(3))
#    data = get_temp_by_city(1)
#    print(list(map(lambda d: d[2], data)))
#    print(get_one_apart_temp_by_city(1, 1, 1))
#    print(get_max_temp_by_city_and_area(4))
    print(get_city_name(1))
