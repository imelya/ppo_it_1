import requests
from config import TOKEN, URL

ERROR_MSG_1 = ''
ERROR_MSG_2 = ''

def get_temp(city, area, house, apart):
    url = f'{URL}/{city}/{area}/{house}/{apart}'
    response = requests.get(url, headers={'X-Auth-Token': TOKEN})
    if not response:
        return ERROR_MSG_1
    data = response.json().get('data')
    if not data:
        return ERROR_MSG_2
    return data.get('temperature')

if __name__ =='__main__':
    print(get_temp(1, 1, 1, 1))
