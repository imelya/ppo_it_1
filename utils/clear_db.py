import sqlite3
from sys import argv, path
path.append('../')
from config import PATH, COUNT
import os

def main(name):
    conn = sqlite3.connect(PATH + 'db/' + name)
    cursor = conn.cursor()

    for i in range(COUNT):
        cursor.execute(f'delete from city_temp where n = {i}')
    for i in range(COUNT):
        cursor.execute(f'delete from apart_temp where n = {i}')
    conn.commit()
    conn.close()

if __name__ == '__main__':
    if len(argv) != 2:
        print('wrong argv')
        exit()
    else:
        if not os.path.exists(PATH + 'db/' + argv[1]):
            print('no db')
            exit()
        else:
            main(argv[1])
