#!/usr/bin/env python3
from sys import argv, path
import sqlite3
import os
import requests
path.append('../')
from config import table_name_1, table_name_2, table_name_3, PATH, URL, TOKEN


def get_city_names_from_service():
    r = requests.get(URL, headers={'X-Auth-Token': TOKEN})
    cities = r.json().get('data')
    return cities

def main(name):
    conn = sqlite3.connect(f'{PATH}db/{name}')
    cursor = conn.cursor()
    cursor.execute(f'CREATE TABLE {table_name_1} (id integer, name text)')
    cursor.execute(f'CREATE TABLE {table_name_2} (n integer, city integer, temp integer)')
    cursor.execute(f'CREATE TABLE {table_name_3} (id integer, city integer, area integer, house integer, apart integer, temp integer)')
    for city in get_city_names_from_service():
        cursor.execute(f'INSERT INTO {table_name_1} values (?, ?)', (city.get('city_id'), city.get('city_name')))
    conn.commit()
    conn.close()

if __name__ == '__main__':
    if len(argv) != 2:
        print('wrong argv')
        exit()
    else:
        if os.path.exists(PATH + 'db/' + argv[1]):
            print('db exists')
            exit()
        else:
            main(argv[1])
