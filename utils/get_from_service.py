import requests
import sqlite3
from time import sleep
from sys import path
PATH = '/home/bot_user/ppo_it_1/'
path.append(PATH)
from config import TOKEN, number_of_areas, number_of_aparts, number_of_houses, COUNT, DELAY, URL

HEADERS = {'X-Auth-Token': TOKEN}

def write_to_db(n, city, temp, area=None, house=None, apart=None):
    conn = sqlite3.connect(PATH + 'db/db_2.db')
    cursor = conn.cursor()
    if not area:
        cursor.execute('insert into city_temp values (?, ?, ?)', (n, city, temp))
        conn.commit()
    else:
        cursor.execute('insert into apart_temp values (?, ?, ?, ?, ?, ?)', (n, city, area, house, apart, temp))
        conn.commit()

for i in range(COUNT):
    for city in range(1, 17):
        response_1 = requests.get(f'{URL}{city}', headers=HEADERS)
        if not response_1:
            pass
        else:
            data_1 = response_1.json()
            if not data_1.get('data'):
                pass
            else:
                temp_1 = data_1.get('data').get('temperature')
                print('temp_1:', temp_1)
                write_to_db(n=i, city=city, temp=temp_1)
        for area in range(1, number_of_areas + 1):
            for house in range(1, number_of_houses + 1):
                for apart in range(1, number_of_aparts + 1):
                    response_2 = requests.get(f'{URL}{city}/{area}/{house}/{apart}', headers=HEADERS)
                    if not response_2:
                        pass
                    else:
                        data_2 = response_2.json()
                        if not data_2.get('data'):
                            pass
                        else:
                            temp_2 = data_2.get('data').get('temperature')
                            print('temp_2:', temp_2)
                            write_to_db(n=i, city=city, area=area, house=house, apart=apart, temp=temp_2)

    sleep(DELAY)
