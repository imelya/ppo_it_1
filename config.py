TOKEN = '3mmaxizktypb2rcz'
number_of_areas = 4 #Кол-во опрашиваемых райнонов
number_of_aparts = 3 #Кол-во опрашиваемых квартир
number_of_houses = 1 #Кол-во опрашиваемых домов
PATH = '/home/bot_user/ppo_it_1/' #Путь в системе к рабочей директории
DB_PATH = PATH + 'db/db_2.db' #Путь к базе данных
table_name_1 = 'cities' #Имя таблицы в БД с именами и id городов
table_name_2 = 'city_temp' #Имя таблицы в БД с данными температуры в городах
table_name_3 = 'apart_temp' #Имя таблицы в БД с данными температуры в квартирах
DELAY = 60 * 7 #7 мин Временной интервал между запросами к сервису
COUNT = 200 #7 мин * 200 = 24 час. Кол-во запросов к сервису
URL = 'https://dt.miet.ru/ppo_it/api/' #Адрес сервиса
